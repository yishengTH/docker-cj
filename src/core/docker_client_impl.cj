package docker_cj.core

import docker_cj.core.command.*

/**
 * @author yishengTH
 * @description DockerClient的实现类
 * @detail 构造时传入docker配置与docker请求客户端，根据配置与请求客户端构造具体的命令工厂，由命令工厂继续实现功能API
 */
public class DockerClientImpl <: DockerClient {
    DockerClientImpl(let dockerClientConfig: DockerClientConfig) {}

    private var dockerCmdExecFactory: ?DockerCmdExecFactory = None

    public static func getInstance(dockerClientConfig: DockerClientConfig, dockerHttpClient: DockerHttpClient): DockerClientImpl {
        return DockerClientImpl(dockerClientConfig).withHttpClient(dockerHttpClient)
    }

    func withHttpClient(dockerHttpClient: DockerHttpClient): DockerClientImpl {
        return withDockerCmdExecFactory(DefaultDockerCmdExecFactory(dockerHttpClient))
    }

    func withDockerCmdExecFactory(dockerCmdExecFactory: DockerCmdExecFactory) {
        this.dockerCmdExecFactory = dockerCmdExecFactory

        if (let Some(aware) <- dockerCmdExecFactory as DockerClientConfigAware) {
            aware.initConfig(dockerClientConfig)
        }

        return this
    }

    private func getDockerCmdExecFactory(): DockerCmdExecFactory {
        return this.dockerCmdExecFactory
            .getOrThrow({=>Exception("dockerCmdExecFactory was not specified")})
    }

    /**
     * @Function Expansion 此处您可以进行功能扩展以支持更多的API
     */

    /**
     * * IMAGE API *  镜像相关API
     */
    public override func listImagesCmd(): ListImagesCmd {
        return ListImagesCmdImpl(getDockerCmdExecFactory().createListImagesCmdExec())
    }

    public override func removeImageCmd(imageId: String): RemoveImageCmd {
        return RemoveImageCmdImpl(getDockerCmdExecFactory().createRemoveImageCmdExec(), imageId)
    }
}
