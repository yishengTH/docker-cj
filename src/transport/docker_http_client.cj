package docker_cj.transport

import std.collection.*

public interface DockerHttpClient <: Resource{
    func execute(request: DockerRequest): IDockerResponse
}

public interface IDockerResponse {
    func getStatusCode(): Int64

    func getHeaders(): Map<String, ArrayList<String>>

    func getBody(): ?String // FUTURE 应该使用InputStream转化为更通用的流 目前受制于第三方库原因还是用String更方便

    func close(): Unit
}

public class DockerRequest {
    DockerRequest(
        public let headers: Map<String, String>,
        public var method: RequestMethod,
        public var path: String,
        public var body: ?String
    ) {}
}

public class DockerRequestBuilder {
    private let headers: Map<String, String> = HashMap<String, String>()
    private var method: RequestMethod = RequestMethodEnum.GET
    private var path: ?String = None
    private var body: ?String = None

    public func withMethod(method: RequestMethod): DockerRequestBuilder {
        this.method = method
        return this
    }

    public func withPath(path: String): DockerRequestBuilder {
        this.path = path
        return this
    }

    public func putHeader(key: String, value: String): DockerRequestBuilder {
        this.headers.put(key, value)
        return this
    }

    public func withBody(bodyVal: String): DockerRequestBuilder {
        this.body = bodyVal
        return this
    }

    public func build(): DockerRequest {
        return DockerRequest(
            this.headers,
            this.method,
            this.path.getOrThrow({=>Exception("The path of DockerRequestBuilder is not set")}),
            this.body
        )
    }
}

public class RequestMethod {
    RequestMethod(public let methodName: String) {}
}

public class RequestMethodEnum {
    public static let GET = RequestMethod("GET")
    public static let POST = RequestMethod("POST")
    public static let DELETE = RequestMethod("DELETE")
}
